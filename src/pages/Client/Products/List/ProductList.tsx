import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Link, useLocation } from "react-router-dom";
import { IProduct } from "../../../../common/interfaces/Product";
import "./style.css";

type ProductListProps = {
  products?: IProduct[];
  title?: string;
  carousel?: boolean;
};

const ProductList = ({
  products,
  title,
  carousel = false,
}: ProductListProps) => {
  const location = useLocation();
  const isHomePage = location.pathname === "/";
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2,
    },
  };
  const formattedPrice = (price: number) => {
    return price.toLocaleString("vi-VN");
  };
  return (
    <div className="xl:container xl:mx-auto">
      <div className="flex  items-center justify-between mt-5 mb-5">
        <h1 className="text-5xl font-bold">{title}</h1>
        <div className="relative">
          <Link
            to={`collections/${title}`}
            className={`reversed-link flex items-center justify-center gap-2 relative ${
              isHomePage ? "block" : "hidden"
            }`}
          >
            <span>Xem thêm</span>
            <div className="bg-slate-300 text-xs px-2 py-1 rounded-full">
              <FontAwesomeIcon icon={faChevronRight} />
            </div>
          </Link>
        </div>
      </div>
      <div
        className={`product-list ${
          !carousel ? "flex flex-wrap " : "justify-center"
        }`}
      >
        {/* product */}
        {carousel ? (
          <Carousel
            autoPlay={true}
            autoPlaySpeed={1000}
            infinite={true}
            removeArrowOnDeviceType={["tablet", "mobile"]}
            responsive={responsive}
          >
            {products?.map((product: IProduct, index: number) => (
              <div key={index} className="product-item m-3">
                <Link to={`/product/${product.slug}`} className="relative">
                  <div
                    className={`discount absolute z-50 bg-red-300 text-white p-2 rounded ${
                      product.discount! > 0 ? "block" : "hidden"
                    }`}
                  >
                    Tiết kiệm{" "}
                    {Math.floor(
                      ((product.price - product.discount!) / product.price!) *
                        100
                    )}
                    %
                  </div>
                  <div className="figure">
                    <img
                      src={product.variants[0].thumbnail[0].imageUrl}
                      alt={`Variant 0 Thumbnail 0`}
                    />
                  </div>
                  <div className="info">
                    <span className="title text-lg font-semibold">
                      {product.name}
                    </span>
                    <div>
                      {product.discount! > 0 ? (
                        <div>
                          <span className="text-red-600">
                            {product.discount!}₫
                          </span>
                          <span className="text-gray-500 line-through">
                            {product.price}₫
                          </span>
                        </div>
                      ) : (
                        <div>
                          <span className="text-gray-800">
                            {product.price}₫
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </Link>
              </div>
            ))}
          </Carousel>
        ) : (
          products?.map((product: IProduct, index: number) => (
            <div key={index} className="product-item m-3">
              <Link to={`/product/${product.slug}`} className="relative">
                <div
                  className={`discount absolute z-50 bg-red-300 text-white p-2 rounded ${
                    product.discount! > 0 ? "block" : "hidden"
                  }`}
                >
                  Tiết kiệm{" "}
                  {Math.floor(
                    ((product.price - product.discount!) / product.price!) * 100
                  )}
                  %
                </div>
                <div className="figure">
                  <img
                    src={product.variants[0].thumbnail[0].imageUrl}
                    alt={`Variant 0 Thumbnail 0`}
                  />
                </div>
                <div className="info">
                  <span className="title text-lg font-semibold">
                    {product.name}
                  </span>
                  <div>
                    {product.discount! > 0 ? (
                      <div>
                        <span className="text-red-600">
                          {product.discount!}
                        </span>
                        <span className="text-gray-500 line-through">
                          {formattedPrice(product.price)}
                        </span>
                      </div>
                    ) : (
                      <div>
                        <span className="text-gray-800">
                          {product.price.toLocaleString()}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
              </Link>
            </div>
          ))
        )}
      </div>
    </div>
  );
};

export default ProductList;
