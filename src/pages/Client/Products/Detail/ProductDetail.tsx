/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useGlobalState } from "../../../../common/context/AppContextProvider";
import useProductQuery from "../../../../common/hooks/Product/useProductQuery";
import { IProduct } from "../../../../common/interfaces/Product";
import Loading from "../../../../components/base/Loading/Loading";
import "./style.css";
const ProductDetail = () => {
  const { slug } = useParams();
  const updateURL = useNavigate();
  const queryParameters = new URLSearchParams(window.location.search);
  const { data, isLoading } = useProductQuery({ slug });
  const [selectedVariant, setSelectedVariant] = useState([]);
  const [quantity, setQuantity] = useState(1);
  const [active, setActive] = useState<string>();
  const { state, dispatch } = useGlobalState();
  const [images, setImages] = useState([]);
  const [slidesImage, setSlidesImage] = useState<any>();
  const formattedPrice = (price: number) => {
    return price.toLocaleString("vi-VN", {
      style: "currency",
      currency: "VND",
    });
  };
  const handleVariantClick = (id: string) => {
    updateURL(`?variants=${id}`);
    setActive(id);
  };
  useEffect(() => {
    if (!data) return data;
    data.variants.map((variant: any) => setImages(variant.thumbnail));
    const variantId = queryParameters.get("variants");
    if (variantId && data && data.variants) {
      const foundVariant = data.variants.find(
        (variant: IProduct) => variant._id === variantId
      );
      setSelectedVariant(foundVariant);
    }
  }, [data, queryParameters]);
  useEffect(() => {
    if (images.length > 0) {
      setSlidesImage(images[0].imageUrl);
    }
  }, [images]);
  console.log(slidesImage);
  const incrementQuantity = () => {
    setQuantity((prevQuantity) => prevQuantity + 1);
    console.log(state);
  };

  const decrementQuantity = () => {
    if (quantity > 1) {
      setQuantity((prevQuantity) => prevQuantity - 1);
    }
  };
  const addToCart = (product: IProduct) => {
    dispatch({ type: "ADD_TO_CART", payload: product });
    console.log(product);
  };
  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <div>ProductDetail</div>
          <div className="grid grid-cols-4 gap-24 m-12  ">
            <div className="col-span-2">
              <img src={slidesImage} alt="" />
              <div className="flex m-2">
                {images.map((image: any, index: number) => (
                  <>
                    <img key={index}
                      className="w-32 h-32 m-2 scroll-mx-2 cursor-pointer overflow-x-auto"
                      src={image.imageUrl}
                      onClick={() => setSlidesImage(image.imageUrl)}
                      alt=""
                    />
                  </>
                ))}
              </div>
            </div>
            <div className="col-span-2">
              <div className="title-product">
                <h1 className="font-extrabold text-5xl  ">{data.name}</h1>
              </div>
              <div className="flex items-center my-5 pb-5 border-b-2">
                {data.discount > 0 ? (
                  <div>
                    <span className="text-red-600 text-2xl">
                      {formattedPrice(
                        data.price - (data.price * data.discount) / 100
                      )}
                    </span>
                    <span className="text-gray-500 line-through text-2xl">
                      {formattedPrice(data.price)}
                    </span>
                  </div>
                ) : (
                  <div>
                    <span className="text-gray-800 text-2xl">
                      {formattedPrice(data.price)}
                    </span>
                  </div>
                )}
                <div
                  className={`discount z-50 bg-red-300 text-sm text-white ml-2 p-2 rounded ${
                    data.discount > 0 ? "block" : "hidden"
                  }`}
                >
                  Tiết kiệm {data.discount}%
                </div>
              </div>
              <div className="color my-5">
                <div className="flex flex-col flex-wrap gap-2">
                  <h2 className="text-xl font-bold mb-2">
                    Color: {selectedVariant.color}
                  </h2>
                  {/* <div className="flex gap-4">
                    {data.variants.map((variant: any) => (
                      <button
                        key={variant._id}
                        className="p-3 border border-gray-300 rounded "
                        style={{ backgroundColor: variant.color }}
                        title={`${variant.color}`}
                        onClick={() => handleVariantClick(variant._id)}
                      ></button>
                    ))}
                  </div> */}
                </div>
              </div>
              <div className="color my-5">
                <div className="flex flex-col flex-wrap gap-2">
                  <h2 className="flex gap-2 items-center text-gray-400">
                    Cỡ:
                    <p className="text-xl text-black">{selectedVariant.size}</p>
                  </h2>
                  <div className="flex gap-4">
                    {data.variants.map((variant: any) => (
                      <button
                        key={variant._id}
                        className={`border border-gray-300 p-3 rounded ${
                          variant._id === active ? "border-black" : ""
                        }`}
                        onClick={() => handleVariantClick(variant._id)}
                      >
                        {variant.size}
                      </button>
                    ))}
                  </div>
                </div>
              </div>
              <div className="quantity my-5">
                <label htmlFor="quantity">Số lượng</label>
                <div className="flex items-center gap-10 text-lg border w-fit px-5 py-2">
                  <button onClick={incrementQuantity}>+</button>
                  <p>{quantity}</p>
                  <button onClick={decrementQuantity}>-</button>
                </div>
              </div>
              <div className="add-to-card border my-5">
                <button
                  className="Add-to-cart w-full py-2 text-white bg-black hover:bg-white hover:border-black hover:text-black transition"
                  onClick={() => addToCart(data)}
                >
                  Thêm vào giỏ hàng
                </button>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};
export default ProductDetail;
