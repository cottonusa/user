import useProductQuery from "../../../common/hooks/Product/useProductQuery";
import Loading from "../../../components/base/Loading/Loading";
import ProductList from "../Products/List/ProductList";
import Banner from "./Banner";
const Home = () => {
  const { data, isLoading } = useProductQuery({ page: 1, limit: 6 });

  console.log(data);
  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <Banner />
          <ProductList
            title={"MLB"}
            products={data}
            carousel={true}
          />
          {/* <ProductList title={"123"} products={displayedProducts} /> */}
        </>
      )}
    </>
  );
};

export default Home;
