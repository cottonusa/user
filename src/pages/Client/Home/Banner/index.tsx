import React from "react";

const Banner = () => {
  return (
    <div className="w-full h-screen relative">
      <img
        className="w-full absolute top-0"
        src="src/resources/images/Banner/banner.webp"
        alt="Cotton USA"
        srcSet="src/resources/images/Banner/banner.webp"
      />
      <div className="w-full absolute inset-0 bg-black opacity-50 transition-opacity duration-300"></div>
    </div>
  );
};

export default Banner;
