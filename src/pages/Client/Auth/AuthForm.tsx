import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { joiResolver } from "@hookform/resolvers/joi";
import Joi from "joi";
import { useState } from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
type AuthFormProps = {
  formType: string;
  onSubmit: SubmitHandler<FieldValues>;
};
type Inputs = {
  firstName?: string;
  lastName?: string;
  email: string;
  password: string;
  confirmPassword?: string;
};
const AuthForm = ({ formType, onSubmit }: AuthFormProps) => {
  const isLogin = formType === "login";
  const [password, setPassword] = useState<boolean>(false);
  const handleTogglePassword = () => {
    setPassword(!password);
  };
  const schema = Joi.object({
    email: Joi.string()
      .email({ tlds: { allow: false } })
      .min(3)
      .required()
      .empty()
      .messages({
        "string.email": "Vui lòng cung cấp địa chỉ email hợp lệ.",
        "any.required": "Email là bắt buộc.",
        "string.min": "Email phải có ít nhất {#limit} ký tự.",
        "string.empty": "Vui lòng nhập đầy đủ thông tin",
      }),
    password: Joi.string().min(6).required().empty().messages({
      "any.required": "Mật khẩu là bắt buộc.",
      "string.empty": "Vui lòng nhập đầy đủ thông tin",
      "string.min": "Mật khẩu phải có ít nhất {#limit} ký tự.",
    }),
    ...(isLogin
      ? {}
      : {
          firstName: Joi.string().required().min(3).empty().messages({
            "any.required": "Họ là bắt buộc.",
            "string.min": "Họ phải có ít nhất {#limit} ký tự.",
            "string.empty": "Vui lòng nhập đầy đủ thông tin",
          }),
          lastName: Joi.string().required().min(3).empty().messages({
            "any.required": "Tên là bắt buộc.",
            "string.min": "Tên phải có ít nhất {#limit} ký tự.",
            "string.empty": "Vui lòng nhập đầy đủ thông tin",
          }),
        }),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({
    resolver: joiResolver(schema),
  });

  return (
    <div className="container flex flex-col mx-auto bg-white rounded-lg">
      <div className="flex justify-center w-full h-full my-auto xl:gap-14 lg:justify-normal md:gap-5 draggable">
        <div className="flex items-center justify-center w-full lg:p-12">
          <div className="flex items-center xl:p-10">
            <form
              className="flex flex-col w-full h-full pb-6 text-center bg-white rounded-3xl"
              onSubmit={handleSubmit(onSubmit)}
            >
              <h3 className="mb-3 text-4xl font-extrabold text-dark-grey-900">
                {isLogin ? "Đăng nhập" : "Đăng ký"}
              </h3>
              <p className="mb-4 text-grey-700">
                Enter your email and password
              </p>
              <a className="flex items-center justify-center w-full py-4 mb-6 text-sm font-medium transition duration-300 rounded-2xl text-grey-900 bg-grey-300 hover:bg-grey-400 focus:ring-4 focus:ring-grey-300">
                <img
                  className="h-5 mr-2"
                  src="https://raw.githubusercontent.com/Loopple/loopple-public-assets/main/motion-tailwind/img/logos/logo-google.png"
                  alt=""
                />
                Sign in with Google
              </a>
              <div className="flex items-center mb-3">
                <hr className="h-0 border-b border-solid border-grey-500 grow" />
                <p className="mx-4 text-grey-600">or</p>
                <hr className="h-0 border-b border-solid border-grey-500 grow" />
              </div>
              <div className={`flex gap-2 ${!isLogin ? "block" : "hidden"}`}>
                <div>
                  <label
                    htmlFor="firstName"
                    className="mb-2 text-sm text-start block w-full text-grey-900"
                  >
                    Họ*
                  </label>
                  <input
                    type="text"
                    placeholder="Họ"
                    className="flex items-center w-full px-5 py-4 mr-2 text-sm font-medium outline-none focus:bg-grey-400 mb-4 placeholder:text-grey-700 bg-grey-200 text-dark-grey-900 rounded-2xl"
                    {...register("firstName", { required: true })}
                  />
                  {errors.firstName && (
                    <span className="text-danger">
                      {errors.firstName.message?.toString()}
                    </span>
                  )}
                </div>
                <div>
                  <label
                    htmlFor="firstName"
                    className="mb-2 text-sm text-start block w-full text-grey-900"
                  >
                    Tên*
                  </label>
                  <input
                    type="text"
                    placeholder="Tên"
                    className="flex items-center w-full px-5 py-4 mr-2 text-sm font-medium outline-none focus:bg-grey-400 mb-4 placeholder:text-grey-700 bg-grey-200 text-dark-grey-900 rounded-2xl"
                    {...register("lastName", { required: true })}
                  />
                  {errors.lastName && (
                    <span>{errors.lastName.message?.toString()}</span>
                  )}
                </div>
              </div>
              <div>
                <label
                  htmlFor="email"
                  className="mb-2 text-sm text-start block w-full text-grey-900"
                >
                  Email*
                </label>
                <input
                  type="email"
                  placeholder="mail@loopple.com"
                  className="flex items-center w-full px-5 py-4 mr-2 text-sm font-medium outline-none focus:bg-grey-400 mb-4 placeholder:text-grey-700 bg-grey-200 text-dark-grey-900 rounded-2xl"
                  {...register("email", { required: true })}
                />
                {errors.email && (
                  <span>{errors.email.message?.toString()}</span>
                )}
              </div>
              <div className="relative">
                <label
                  htmlFor="password"
                  className="mb-2 w-full block text-sm text-start text-grey-900"
                >
                  Password*
                </label>
                <input
                  id="password"
                  type={password ? "text" : "password"}
                  placeholder="Enter a password"
                  className="flex items-center w-full px-5 py-4 mb-5 mr-2 text-sm font-medium outline-none focus:bg-grey-400 placeholder:text-grey-700 bg-grey-200 text-dark-grey-900 rounded-2xl"
                  {...register("password", { required: true })}
                />
                {errors.password && (
                  <span>{errors.password.message?.toString()}</span>
                )}
                <FontAwesomeIcon
                  className="absolute inset-y-0 right-4 top-12"
                  icon={password ? faEyeSlash : faEye}
                  onClick={handleTogglePassword}
                />
              </div>
              <div
                className={`flex flex-row justify-between mb-8 ${
                  isLogin ? "block" : "hidden"
                }`}
              >
                <label className="relative inline-flex items-center mr-3 cursor-pointer select-none">
                  <input
                    type="checkbox"
                    defaultValue=""
                    className="sr-only peer"
                  />
                  <div className="w-5 h-5 bg-white border-2 rounded-sm border-grey-500 peer peer-checked:border-0 peer-checked:bg-purple-blue-500">
                    <img
                      className=""
                      src="https://raw.githubusercontent.com/Loopple/loopple-public-assets/main/motion-tailwind/img/icons/check.png"
                      alt="tick"
                    />
                  </div>
                  <span className="ml-3 text-sm font-normal text-grey-900">
                    Keep me logged in
                  </span>
                </label>
                <a
                  href="#"
                  className="mr-4 text-sm font-medium text-purple-blue-500"
                >
                  Forget password?
                </a>
              </div>
              <button className="w-full px-6 py-5 mb-5 text-sm font-bold leading-none text-white transition duration-300 rounded-2xl hover:bg-purple-blue-600 focus:ring-4 focus:ring-purple-blue-100 bg-purple-blue-500">
                {isLogin ? "Đăng nhập" : "Đăng ký"}
              </button>
              <p className="text-sm leading-relaxed text-grey-900">
                {isLogin ? "Bạn đã có tài khoản ?" : "Bạn chưa có tài khoản ?"}

                <Link
                  to={isLogin ? "/sign-up" : "/sign-in"}
                  className="font-bold text-grey-700 underline underline-offset-4"
                >
                  {" "}
                  {isLogin ? "Đăng ký ngay" : "Đăng nhập ngay"}
                </Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuthForm;
