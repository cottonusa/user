/* eslint-disable @typescript-eslint/no-explicit-any */
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { joiResolver } from "@hookform/resolvers/joi";
import { jwtDecode } from "jwt-decode";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import { useGlobalState } from "../../../common/context/AppContextProvider";
import { IAuth } from "../../../common/interfaces/Auth";
import { AuthSignIn, getById } from "../../../services/auth";
import { SignInSchema } from "../../../common/validations/auth";
import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";

const SignIn = () => {
  const navigate = useNavigate();
  const [password, setPassword] = useState<boolean>(false);
  const { dispatch } = useGlobalState();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IAuth>({ resolver: joiResolver(SignInSchema) });
  const handleTogglePassword = () => {
    setPassword(!password);
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const setUser = (user: any) => ({
    type: "SET_USER",
    payload: user,
  });
  const onSubmit = async (data: IAuth) => {
    try {
      const res = await AuthSignIn(data);
      console.log(res);
      localStorage.setItem("token", res.accessToken);
      if (res.accessToken) {
        const decoded: any = jwtDecode(res.accessToken);
        if (decoded?._id) {
          const res = await getById(decoded._id);
          dispatch(setUser(res));
          console.log(res);
        }
      }
      toast.success("Đăng nhập thành công", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <>
        <div className="container flex flex-col mx-auto bg-white rounded-lg pt-12 my-5">
          <div className="flex justify-center w-full h-full my-auto xl:gap-14 lg:justify-normal md:gap-5 draggable">
            <div className="flex items-center justify-center w-full lg:p-12">
              <div className="flex items-center xl:p-10">
                <form
                  className="flex flex-col w-full h-full pb-6 text-center bg-white rounded-3xl"
                  onSubmit={handleSubmit(onSubmit)}
                >
                  <h3 className="mb-3 text-4xl font-extrabold text-dark-grey-900">
                    Sign In
                  </h3>
                  <p className="mb-4 text-grey-700">
                    Enter your email and password
                  </p>

                  <div>
                    <label
                      htmlFor="email"
                      className="mb-2 text-sm text-start block w-full text-grey-900"
                    >
                      Email*
                    </label>
                    <input
                      type="email"
                      placeholder="mail@loopple.com"
                      className="flex items-center w-full px-5 py-4 mr-2 text-sm font-medium outline-none focus:bg-grey-400 mb-7 placeholder:text-grey-700 bg-grey-200 text-dark-grey-900 rounded-2xl"
                      {...register("email", { required: true })}
                    />
                    {errors.email && <span>{errors.email.message}</span>}
                  </div>
                  <div className="relative">
                    <label
                      htmlFor="password"
                      className="mb-2 w-full block text-sm text-start text-grey-900"
                    >
                      Password*
                    </label>
                    <input
                      id="password"
                      type={password ? "text" : "password"}
                      placeholder="Enter a password"
                      className="flex items-center w-full px-5 py-4 mb-5 mr-2 text-sm font-medium outline-none focus:bg-grey-400 placeholder:text-grey-700 bg-grey-200 text-dark-grey-900 rounded-2xl"
                      {...register("password", { required: true })}
                    />
                    {errors.password && <span>{errors.password.message}</span>}
                    <FontAwesomeIcon
                      className="absolute inset-y-0 right-4 top-12"
                      icon={password ? faEyeSlash : faEye}
                      onClick={handleTogglePassword}
                    />
                  </div>
                  <div className="flex flex-row justify-between mb-8">
                    <label className="relative inline-flex items-center mr-3 cursor-pointer select-none">
                      <input
                        type="checkbox"
                        defaultValue=""
                        className="sr-only peer"
                      />
                      <div className="w-5 h-5 bg-white border-2 rounded-sm border-grey-500 peer peer-checked:border-0 peer-checked:bg-purple-blue-500">
                        <img
                          className=""
                          src="https://raw.githubusercontent.com/Loopple/loopple-public-assets/main/motion-tailwind/img/icons/check.png"
                          alt="tick"
                        />
                      </div>
                      <span className="ml-3 text-sm font-normal text-grey-900">
                        Keep me logged in
                      </span>
                    </label>
                    <a
                      href="#"
                      className="mr-4 text-sm font-medium text-purple-blue-500"
                    >
                      Forget password?
                    </a>
                  </div>
                  <button className="w-full px-6 py-5 mb-5 text-sm font-bold leading-none text-white transition duration-300 md:w-96 rounded-2xl hover:bg-purple-blue-600 focus:ring-4 focus:ring-purple-blue-100 bg-purple-blue-500">
                    Sign In
                  </button>
                  <p className="text-sm leading-relaxed text-grey-900">
                    Bạn chưa có tài khoản ?
                    <Link
                      to="/sign-up"
                      className="font-bold text-grey-700 underline underline-offset-4"
                    >
                      Đăng ký ngay
                    </Link>
                  </p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </>
    </div>
  );
};

export default SignIn;
