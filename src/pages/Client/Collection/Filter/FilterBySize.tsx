import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import { useEffect, useState } from "react";


const FilterBySize = ({
  onChange,
}: {
  onChange: (size: string[]) => void;
}) => {
  const [showMore, setShowMore] = useState<boolean>(false);
  const [selectedSizes, setSelectedSizes] = useState<string[]>([]);
  const handleToggleOption = () => {
    setShowMore(!showMore);
  };
  const size = ["XS", "S", "M", "L", "XL", "XXL"];
  const handleSizeChange = (size: string) => {
    if (selectedSizes.includes(size)) {
      setSelectedSizes(selectedSizes.filter((item) => item !== size));
    } else {
      setSelectedSizes([...selectedSizes, size]);
    }
  };
  useEffect(() => {
    onChange(selectedSizes);
  }, [onChange, selectedSizes]);
  return (
    <div className="pt-3 border-y border-gray-300">
      <div className="relative">
        <button
          className="flex items-center justify-between font-medium mb-3 w-full hover:text-amber-800"
          onClick={handleToggleOption}
        >
          <span>Size</span>
          <FontAwesomeIcon
            className="ml-2 p-1 rounded-xl text-xs w-auto transition"
            icon={showMore ? faChevronUp : faChevronDown}
          />
        </button>
      </div>
      {showMore && (
        <div className="ml-4 mb-4">
          <FormGroup>
            {size.map((item, index) => (
              <FormControlLabel
                key={index}
                control={
                  <Checkbox
                    checked={selectedSizes.includes(item)}
                    onChange={() => handleSizeChange(item)}
                  />
                }
                label={item}
              />
            ))}
          </FormGroup>
        </div>
      )}
    </div>
  );
};

export default FilterBySize;
