import { Box, Button, TextField, Typography } from "@mui/material";
import { useState } from "react";

interface FilterByPriceProps {
  onChange: (value: { price_gte: number; price_lte: number }) => void;
}

const FilterByPrice: React.FC<FilterByPriceProps> = ({ onChange }) => {
  const [value, setValue] = useState({
    price_gte: 0,
    price_lte: 0,
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    const numericValue = value.replace(/\D/g, "");
    console.log(numericValue);
    setValue((prevValues) => ({
      ...prevValues,
      [name]: Number(numericValue),
    }));
  };

  const handleSubmit = () => {
    if (onChange) onChange(value);
    setValue({
      price_gte: 0,
      price_lte: 0,
    });
  };

  return (
    <Box>
      <Typography>Chọn khoảng giá</Typography>
      <div className="flex items-center">
        <TextField
          name="price_gte"
          value={value.price_gte}
          onChange={handleChange}
        />
        <p className="px-2"> - </p>
        <TextField
          name="price_lte"
          value={value.price_lte}
          onChange={handleChange}
        />
      </div>
      <Button size="small" color="primary" onClick={handleSubmit}>
        Áp dụng
      </Button>
    </Box>
  );
};

export default FilterByPrice;
