/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import useProductQuery from "../../../common/hooks/Product/useProductQuery";
import { IProduct } from "../../../common/interfaces/Product";
import Loading from "../../../components/base/Loading/Loading";
import ProductList from "../Products/List/ProductList";
import FilterByPrice from "./Filter/FilterByPrice";
import FilterBySize from "./Filter/FilterBySize";

const Collection = () => {
  const { slug } = useParams();
  const [sortBy, setSortBy] = useState("");
  const { data, isLoading } = useProductQuery({ page: 1, limit: 6 });
  const [size, setSize] = useState<string[]>([]);
  const [value, setValue] = useState<string>("bestseller");
  const [price, setPrice] = useState<{
    price_gte: number;
    price_lte: number;
  }>();
  const [products, setProducts] = useState<IProduct[]>([]);
  useEffect(() => {
    const applyFilters = () => {
      if (!products) return;
      let filteredProducts = [...products];
      if (price && price.price_gte && price.price_lte) {
        filteredProducts = filteredProducts.filter((product) => {
          return (
            product.price >= price.price_gte && product.price <= price.price_lte
          );
        });
        setProducts(filteredProducts);
      }
    };
    applyFilters();
  }, [size, price, sortBy, products]);

  const handleFilterSize = (sizeFilter: string[]) => {
    setSize(sizeFilter);
  };

  const handleFilterPrice = (value: {
    price_gte: number;
    price_lte: number;
  }) => {
    setPrice(value);
  };

  useEffect(() => {
    if (!data) return;

    const productsInCategory = [...data];
    let filterProductInCategory = [];

    if (slug === "t-shirt") {
      filterProductInCategory = productsInCategory.filter(
        (product: IProduct) =>
          product.category?.name.toLocaleLowerCase() ===
          slug.toLocaleLowerCase()
      );
    } else {
      filterProductInCategory = productsInCategory.filter(
        (product: IProduct) => product.collections?.slug === slug
      );
    }

    setProducts(filterProductInCategory);
  }, [data, slug]);

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <div className="m-3">
          <div className="flex items-baseline justify-between pb-6 pt-24">
            <span className="text-4xl font-bold tracking-tight text-gray-900">
              Bộ lọc
            </span>
            <div>
              <form className="flex items-center max-w-sm mx-auto">
                <span className="text-lg font-semibold pr-2">
                  Sắp xếp theo:
                </span>
                <select
                  id="sort"
                  className="p-2 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500"
                  onChange={(event) => setValue(event.target.value)}
                  value={value}
                >
                  <option value="bestseller">Bán chạy nhất</option>
                  <option value="highestPrice">Giá: cao nhất</option>
                  <option value="lowestPrice">Giá: thấp nhất</option>
                  <option value="oldest">Cũ nhất</option>
                  <option value="latest">Mới nhất</option>
                  <option value="a-z">Tên: A - Z</option>
                  <option value="z-a">Tên: Z - A</option>
                </select>
              </form>
            </div>
          </div>
          <div className="grid grid-cols-1 gap-x-8 gap-y-10 lg:grid-cols-4">
            <div className="col-span-1">
              <FilterBySize onChange={handleFilterSize} />
              <FilterByPrice onChange={handleFilterPrice} />
            </div>
            <div className="col-span-3">
              <ProductList />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Collection;
