import { useEffect, useState } from "react";

const Sort = ({
  onChangeSort,
}: {
  onChangeSort: (sortOption: string) => void;
}) => {
  const [value, setValue] = useState<string>("bestseller");
  useEffect(() => {
      onChangeSort(value);
  }, [onChangeSort, value]);
  return (
    <div>
      <div className="flex items-center justify-between">
        <div className="flex items-center">
          <svg
            role="presentation"
            fill="none"
            focusable="false"
            strokeWidth="2"
            width="20"
            height="14"
            className="icon-subdued icon icon-filter"
            viewBox="0 0 20 14"
          >
            <path
              d="M1 2C0.447715 2 0 2.44772 0 3C0 3.55228 0.447715 4 1 4V2ZM1 4H5V2H1V4Z"
              fill="currentColor"
            ></path>
            <path
              d="M1 10C0.447715 10 0 10.4477 0 11C0 11.5523 0.447715 12 1 12V10ZM1 12H11V10H1V12Z"
              fill="currentColor"
            ></path>
            <path
              d="M10 2H9V4H10V2ZM19 4C19.5523 4 20 3.55228 20 3C20 2.44772 19.5523 2 19 2V4ZM10 4H19V2H10V4Z"
              fill="currentColor"
            ></path>
            <path
              d="M16 10H15V12H16V10ZM19 12C19.5523 12 20 11.5523 20 11C20 10.4477 19.5523 10 19 10V12ZM16 12H19V10H16V12Z"
              fill="currentColor"
            ></path>
            <circle cx="7" cy="3" r="2" stroke="currentColor"></circle>
            <circle cx="13" cy="11" r="2" stroke="currentColor"></circle>
          </svg>
          <span className="pl-2">Bộ lọc</span>
        </div>
        <div>
          <form className="flex items-center max-w-sm mx-auto">
            <span className="text-lg font-semibold pr-2">Sắp xếp theo:</span>
            <select
              id="sort"
              className=" p-2 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500"
              onChange={(event) => setValue(event.target.value)}
              defaultValue={value}
            >
              <option className="p-2 fixed " value="bestseller">
                Bán chạy nhất
              </option>
              <option value="highestPrice">Giá: cao nhất</option>
              <option value="lowestPrice">Giá: thấp nhất</option>
              <option value="oldest">Cũ nhất</option>
              <option value="latest">Mới nhất</option>
              <option value="a-z">Tên: A - Z</option>
              <option value="z-a">Tên: Z - A</option>
            </select>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Sort;
