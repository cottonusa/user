/* eslint-disable @typescript-eslint/no-explicit-any */
import { faTrashCan } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { X } from "lucide-react";
import { ChangeEvent, useState } from "react";
import { FieldErrors, UseFormRegister } from "react-hook-form";
import { Input } from "../../../../components/ui/Input";
import FileUploadButton from "./UploadImage";
interface SizeQuantity {
  size: string;
  quantity: string;
}

interface ClassificationGroup {
  color: string;
  thumbnail: string[];
  option: SizeQuantity[];
}
interface ClassificationGroupsProps {
  register: UseFormRegister<any>;
  errors: FieldErrors<any>;
}

const ClassificationGroups = ({ register }: ClassificationGroupsProps) => {
  const [classificationGroups, setClassificationGroups] = useState<
    ClassificationGroup[]
  >([{ color: "", thumbnail: [], option: [{ size: "", quantity: "" }] }]);
  const handleAddOption = (groupIndex: number) => {
    const updatedGroups = [...classificationGroups];
    updatedGroups[groupIndex].option.push({ size: "", quantity: "" });
    setClassificationGroups(updatedGroups);
  };

  const handleRemoveOption = (groupIndex: number, optionIndex: number) => {
    const updatedGroups = [...classificationGroups];
    updatedGroups[groupIndex].option.splice(optionIndex, 1);
    setClassificationGroups(updatedGroups);
  };

  const handleClassificationGroupChange = (
    groupIndex: number,
    optionIndex: number,
    field: keyof SizeQuantity | "color",
    value: string
  ) => {
    const updatedGroups = [...classificationGroups];
    if (field === "color") {
      updatedGroups[groupIndex].color = value;
    } else {
      updatedGroups[groupIndex].option[optionIndex][
        field as keyof SizeQuantity
      ] = value;
    }
    setClassificationGroups(updatedGroups);
  };

  const handleRemoveClassificationGroup = (index: number) => {
    const updatedGroups = classificationGroups.filter(
      (_, idx) => idx !== index
    );
    setClassificationGroups(updatedGroups);
  };
  const handleImage = (images: string[], groupIndex: number) => {
    const updatedGroups = [...classificationGroups];
    updatedGroups[groupIndex].thumbnail = images;
    setClassificationGroups(updatedGroups);
  };
  return (
    <div className="sm:col-span-full">
      <label
        htmlFor="ProductClassification"
        className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
      >
        Phân loại sản phẩm
      </label>
      <button
        type="button"
        onClick={() => {
          const newGroup = {
            color: "",
            thumbnail: [],
            option: [{ size: "", quantity: "" }],
          };
          setClassificationGroups([...classificationGroups, newGroup]);
        }}
        className="my-2 px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600"
      >
        Thêm nhóm phân loại
      </button>
      <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
          <div className="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg">
            <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
              <thead className="bg-gray-50 dark:bg-gray-800">
                <tr>
                  <th className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    Màu
                  </th>
                  <th className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <span className="mt-10 grid grid-cols-1 items-center gap-x-6 gap-y-8 sm:grid-cols-6">
                      <p className="sm:col-span-3">Kích cỡ</p>
                      <p className="sm:col-span-3">Kho hàng</p>
                    </span>
                  </th>
                  <th className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    Chức năng
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700">
                {classificationGroups.map((group, groupIndex) => (
                  <tr key={groupIndex}>
                    <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                      <div className="my-2">
                        <Input
                          className="border-b"
                          // value={group.color}
                          {...register(`variants.${groupIndex}.color`, {
                            required: "Vui lòng nhập màu sản phẩm",
                          })}
                          onChange={(e: ChangeEvent<HTMLInputElement>) =>
                            handleClassificationGroupChange(
                              groupIndex,
                              0,
                              "color",
                              e.target.value
                            )
                          }
                          placeholder="Nhập màu sản phẩm"
                        />
                      </div>
                      <FileUploadButton
                        onChange={handleImage}
                        groupIndex={groupIndex}
                        register={register}
                      />
                    </td>
                    <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                      {group.option.map((sizeObj, optionIndex) => (
                        <div
                          className="flex justify-between my-2"
                          key={optionIndex}
                        >
                          <Input
                            className="border-b mr-2"
                            {...register(
                              `variants.${groupIndex}.option.${optionIndex}.size`
                            )}
                            onChange={(e: ChangeEvent<HTMLInputElement>) =>
                              handleClassificationGroupChange(
                                groupIndex,
                                optionIndex,
                                "size",
                                e.target.value
                              )
                            }
                            placeholder="Size"
                          />
                          <Input
                            className="border-b"
                            {...register(
                              `variants.${groupIndex}.option.${optionIndex}.quantity`
                            )}
                            onChange={(e: ChangeEvent<HTMLInputElement>) =>
                              handleClassificationGroupChange(
                                groupIndex,
                                optionIndex,
                                "quantity",
                                e.target.value
                              )
                            }
                            placeholder="Quantity"
                          />
                          {optionIndex > 0 && (
                            <button
                              type="button"
                              onClick={() =>
                                handleRemoveOption(groupIndex, optionIndex)
                              }
                            >
                              <X />
                            </button>
                          )}
                        </div>
                      ))}
                      <button
                        type="button"
                        onClick={() => handleAddOption(groupIndex)}
                        className="my-2 px-3 py-1 bg-blue-500 text-white rounded-md hover:bg-blue-600"
                      >
                        Thêm kích thước
                      </button>
                    </td>
                    <td className="px-4 py-4 text-sm text-gray-500 text-center dark:text-gray-300 whitespace-nowrap">
                      {classificationGroups.length > 1 && (
                        <button
                          type="button"
                          onClick={() =>
                            handleRemoveClassificationGroup(groupIndex)
                          }
                        >
                          <FontAwesomeIcon icon={faTrashCan} />
                        </button>
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ClassificationGroups;
