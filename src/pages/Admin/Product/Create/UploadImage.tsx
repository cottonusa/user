/* eslint-disable @typescript-eslint/no-explicit-any */
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import Dialogs from "../../../../components/base/Dialogs/Dialog";
import { UseFormRegister } from "react-hook-form";

const FileUploadButton = ({
  onChange,
  groupIndex,
  register,
}: {
  onChange: (images: string[], groupIndex: number) => void;
  groupIndex: number;
  register: UseFormRegister<any>;
}) => {
  const [files, setFiles] = useState<string[]>([]);
  const [showDialog, setShowDialog] = useState(false);
  const [invalidFiles, setInvalidFiles] = useState({ name: "" });

  const handleUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const uploadedFiles = e.target.files;
    if (uploadedFiles) {
      const newFiles: string[] = [];
      for (let i = 0; i < uploadedFiles.length; i++) {
        const file = uploadedFiles[i];
        setInvalidFiles(file);
        if (file.size > 3 * 1024 * 1024) {
          setShowDialog(true);
        } else {
          const fileUrl = URL.createObjectURL(file);
          newFiles.push(fileUrl);
        }
      }
      setFiles([...files, ...newFiles]);
    }
  };

  const handleDelete = (index: number) => {
    const updatedFiles = files.filter((_, i) => i !== index); // _ là tham số không cần sử dụng
    setFiles(updatedFiles);
  };

  const handleCloseDialogs = () => {
    setShowDialog(false);
  };

  useEffect(() => {
    onChange(files, groupIndex);
  }, [files, groupIndex]);

  return (
    <>
      <span>Hình ảnh: {files.length}/3</span>
      <div className="mt-2 w-fit flex justify-center ">
        <input
          type="file"
          accept="image/*"
          className="sr-only"
          id={`file-upload-${groupIndex}`}
          {...register(`variants.${groupIndex}.thumbnail`)}
          onChange={handleUpload}
          disabled={files.length === 3}
          multiple
        />
        {files.map((file, index) => (
          <div
            key={file}
            className="relative mr-2 mb-2 group rounded-lg border-2 border-dashed border-gray-300/25 p-1"
          >
            <img
              src={file}
              alt=""
              className="h-12 w-12 object-cover rounded-lg"
            />
            <button
              type="button"
              onClick={() => handleDelete(index)}
              className="absolute inset-x-0 bottom-2 text-white rounded-full p-1 opacity-0 group-hover:opacity-100 transition-opacity duration-300 "
            >
              <FontAwesomeIcon icon={faTrash} />
            </button>
          </div>
        ))}
        {files.length < 3 && (
          <label
            htmlFor={`file-upload-${groupIndex}`}
            className="flex justify-center items-center mb-3"
          >
            <div className="p-2 h-12 w-12 flex justify-center items-center flex-col text-sm leading-6 text-gray-600 cursor-pointer rounded-lg border-2 border-dashed border-gray-300/25">
              <span className="text-lg">+</span>
            </div>
          </label>
        )}
      </div>
      {showDialog && (
        <Dialogs
          title={"Lưu ý"}
          value={`Không thể tải tập tin ${invalidFiles.name}: Vui lòng kiểm tra lại định dạng file (chỉ hỗ trợ jpeg, png, jpg). Kích thước tập tin vượt quá 3.0 MB.`}
          openDialog={showDialog}
          buttonCancel={"Xác nhận"}
          onClose={handleCloseDialogs}
        />
      )}
    </>
  );
};

export default FileUploadButton;
