import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

const MainContent = () => {
  return (
    <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
      <Typography paragraph>
        {/* Your main content text */}
      </Typography>
      <Typography paragraph>
        {/* More main content text */}
      </Typography>
    </Box>
  );
};

export default MainContent;
