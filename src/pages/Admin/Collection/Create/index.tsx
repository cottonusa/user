// SwipeableTemporaryDrawer.tsx
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import { X } from "lucide-react";
import * as React from "react";
import CollectionComponent from "./Create";
import { ICategory } from "../../../../common/interfaces/Category";

type Anchor = "bottom";
interface CollectionUpdateProps {
  category: ICategory[];
}
export default function CollectionCreate({ category }: CollectionUpdateProps) {
  const [state, setState] = React.useState({
    bottom: false,
  });
  const [isUpdateComponentVisible, setIsUpdateComponentVisible] =
    React.useState(false);

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
      if (open) {
        setIsUpdateComponentVisible(true);
      } else {
        setIsUpdateComponentVisible(false);
      }
    };
  const anchor: Anchor = "bottom";
  return (
    <div>
      <Button
        sx={{
          color: "inherit",
          padding: 0,
          minWidth: "auto",
          textTransform: "capitalize",
        }}
        onClick={toggleDrawer(anchor, true)}
      >
        <div className=" bg-blue-400 text-white hover:bg-blue-700 transition px-3 py-2 rounded">
          Thêm nhãn hàng
        </div>
      </Button>
      <SwipeableDrawer
        id="drawer"
        anchor={anchor}
        open={state[anchor]}
        onClose={toggleDrawer(anchor, false)}
        onOpen={toggleDrawer(anchor, true)}
      >
        <Box className="p-5 m-4 relative">
          <div
            className="absolute top-0 right-0 cursor-pointer"
            onClick={toggleDrawer(anchor, false)}
          >
            <X />
          </div>
          {isUpdateComponentVisible && (
            <CollectionComponent categoryData={category} />
          )}
        </Box>
      </SwipeableDrawer>
    </div>
  );
}
