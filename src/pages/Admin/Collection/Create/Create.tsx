import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import useCollectionMutation from "../../../../common/hooks/Collection/useCollectionMutation";
import { ICategory } from "../../../../common/interfaces/Category";
import { ICollection } from "../../../../common/interfaces/Collection";
import Message from "../../../../components/base/Message/Message";
import { Input } from "../../../../components/ui/Input";
import MultipleSelect from "../../../../components/ui/MultipleSelect";
type Collection = {
  categoryData: ICategory[];
};
const CollectionComponent = ({ categoryData }: Collection) => {
  // const { data } = useCategoryQuery();
  const [category, setCategory] = useState<ICategory[]>();
  const [showMessage, setShowMessage] = useState(false);
  const [selectedCategories, setSelectedCategories] = useState<string[]>([]);

  useEffect(() => {
    if (!categoryData) return;
    setCategory(categoryData);
  }, [categoryData, setCategory]);
  const handleCategorySelect = (selectedCategory: ICategory[]) => {
    const getID = selectedCategory
      .map((category) => category._id)
      .filter((id) => id !== undefined) as string[];
    setSelectedCategories(getID);
  };
  const { onSubmit, isPending } = useCollectionMutation({
    action: "CREATE",
  });
  const handleSubmitForm = async (data: ICollection) => {
    console.log(selectedCategories);
    console.log(data);
    try {
      await onSubmit(data);
      setShowMessage(true);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    setValue("category", selectedCategories);
  }, [selectedCategories]);
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm();
  return (
    <div>
      <Message
        message={"Thêm nhãn hàng thành công !"}
        timeout={5000}
        openMessage={showMessage}
        type={"success"}
      />
      <form onSubmit={handleSubmit(handleSubmitForm)}>
        <div className="space-y-12">
          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-base font-semibold leading-7 text-gray-900">
              Thêm nhãn hàng
            </h2>
            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
              <div className="sm:col-span-2">
                <label
                  htmlFor="productName"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Tên nhãn hàng
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <Input
                      type="text"
                      placeholder="Nhập tên nhãn hàng..."
                      {...register("name", { required: true })}
                    />
                  </div>
                  <p>
                    {errors.name && <span>Vui lòng không được để trống</span>}
                  </p>
                </div>
              </div>
              <div className="sm:col-span-2">
                <label
                  htmlFor="productName"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Danh mục
                </label>
                <div className="mt-2">
                  <MultipleSelect
                    data={category}
                    onCategorySelect={handleCategorySelect}
                  />
                  <input
                    className="hidden"
                    {...register("category", { required: true })}
                  />
                </div>
                <p>
                  {errors.category && <span>Vui lòng không được để trống</span>}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-6 flex items-center justify-end gap-x-6">
          <button className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
            {isPending ? "Đang thêm" : "Xác nhận"}
          </button>
        </div>
      </form>
    </div>
  );
};

export default CollectionComponent;
