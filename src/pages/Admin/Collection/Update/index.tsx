import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import { X } from "lucide-react";
import * as React from "react";
import { ICollection } from "../../../../common/interfaces/Collection";
import UpdateComponent from "./Update";
import { ICategory } from "../../../../common/interfaces/Category";

type Anchor = "bottom";
interface CollectionUpdateProps {
  id?: string;
  data: ICollection[];
  categoryData: ICategory[];
}

export default function CollectionUpdate({
  id,
  data,
  categoryData,
}: CollectionUpdateProps) {
  const [state, setState] = React.useState({
    bottom: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }
      setState({ ...state, [anchor]: open });
    };

  const anchor: Anchor = "bottom";

  return (
    <div>
      <Button
        sx={{
          color: "inherit",
          padding: 0,
          minWidth: "auto",
          textTransform: "capitalize",
        }}
        onClick={toggleDrawer(anchor, true)}
      >
        Sửa
      </Button>
      <SwipeableDrawer
        id="drawer"
        anchor={anchor}
        open={state[anchor]}
        onClose={toggleDrawer(anchor, false)}
        onOpen={toggleDrawer(anchor, true)}
      >
        <Box className="p-5 m-4 relative" sx={{ height: 260 }}>
          <div
            className="absolute top-0 right-0 cursor-pointer"
            onClick={toggleDrawer(anchor, false)}
          >
            <X />
          </div>
          <UpdateComponent id={id} data={data} categoryData={categoryData} />
        </Box>
      </SwipeableDrawer>
    </div>
  );
}
