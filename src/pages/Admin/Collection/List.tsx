/* eslint-disable @typescript-eslint/no-explicit-any */
import { format } from "date-fns";
import { useState } from "react";
import useCollectionMutation from "../../../common/hooks/Collection/useCollectionMutation";
import useCollectionQuery from "../../../common/hooks/Collection/useCollectionQuery";
import { ICategory } from "../../../common/interfaces/Category";
import { ICollection } from "../../../common/interfaces/Collection";
import Dialogs from "../../../components/base/Dialogs/Dialog";
import Loading from "../../../components/base/Loading/Loading";
import CollectionCreate from "./Create";
import CollectionUpdate from "./Update/index";
import useCategoryQuery from "../../../common/hooks/Category/useCategoryQuery";

const Collection = () => {
  const { data: category } = useCategoryQuery();
  const { data, isLoading } = useCollectionQuery();
  const [showDialog, setShowDialog] = useState(false);
  const [remove, setRemove] = useState<ICollection>();
  const formatDate = (dateString: any) => {
    const date = new Date(dateString);
    return format(date, "HH:mm dd/MM/yyyy");
  };
  const { mutate } = useCollectionMutation({ action: "DELETE" });
  const handleRemove = (collection: ICollection) => {
    setShowDialog(true);
    setRemove(collection);
  };
  const handleSubmitDialogs = () => {
    if (showDialog && remove) {
      console.log(remove);
      mutate(remove);
      setShowDialog(false);
    }
  };
  const handleCloseDialogs = () => {
    setShowDialog(false);
  };
  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          {showDialog && (
            <Dialogs
              title={"Xác nhận"}
              value={"Bạn có muốn xóa brand này không ?"}
              openDialog={showDialog}
              buttonCancel={"Hủy bỏ"}
              buttonSubmit="Đồng ý"
              onClose={handleCloseDialogs}
              onSubmit={handleSubmitDialogs}
            />
          )}
          <div className="flex flex-col mt-5">
            <div className="flex relative justify-between items-center mb-3">
              <h1 className="text-lg">Danh sách nhãn hàng</h1>
              <CollectionCreate category={category} />
            </div>
            <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                <div className="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg">
                  <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                    <thead className="bg-gray-50 dark:bg-gray-800">
                      <tr>
                        <th
                          scope="col"
                          className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                        >
                          <div className="flex items-center gap-x-3">
                            <input
                              type="checkbox"
                              className="text-blue-500 border-gray-300 rounded dark:bg-gray-900 dark:ring-offset-gray-900 dark:border-gray-700"
                            />
                            <button className="flex items-center gap-x-2">
                              <span>STT</span>
                            </button>
                          </div>
                        </th>
                        <th
                          scope="col"
                          className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                        >
                          Tên nhãn hàng
                        </th>
                        <th
                          scope="col"
                          className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                        >
                          Sản phẩm
                        </th>
                        <th
                          scope="col"
                          className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                        >
                          Danh mục
                        </th>
                        <th
                          scope="col"
                          className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                        >
                          Ngày tạo
                        </th>
                        <th
                          scope="col"
                          className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                        >
                          Ngày chỉnh sửa
                        </th>
                        <th
                          scope="col"
                          className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                        >
                          Chức năng
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
                      {data?.map((collection: ICollection, index: number) => (
                        <tr key={index + 1}>
                          <td className="px-4 py-4 text-sm font-medium text-gray-700 dark:text-gray-200 whitespace-nowrap">
                            <div className="inline-flex items-center gap-x-3">
                              <input
                                type="checkbox"
                                className="text-blue-500 border-gray-300 rounded dark:bg-gray-900 dark:ring-offset-gray-900 dark:border-gray-700"
                              />
                              <span>{index + 1}</span>
                            </div>
                          </td>
                          <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                            {collection.name}
                          </td>
                          <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                            Có {collection.products?.length} sản phẩm
                          </td>
                          <td
                            className={`px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap`}
                          >
                            <div className="inline-flex justify-center items-center px-3 py-1 rounded-full gap-x-2 text-emerald-500 bg-emerald-100/60 dark:bg-gray-800">
                              <h2 className="text-sm font-normal">
                                {Array.isArray(collection.category) &&
                                collection.category.length > 0
                                  ? collection.category
                                      .map((i: ICategory) => i.name)
                                      .join(", ")
                                  : "Chưa có danh mục"}
                              </h2>
                            </div>
                          </td>
                          <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                            {formatDate(collection.createdAt)}
                          </td>
                          <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                            {formatDate(collection.updatedAt)}
                          </td>

                          <td className="px-4 py-4 text-sm whitespace-nowrap">
                            <div className="flex items-center gap-x-6">
                              <div className="text-gray-500 transition-colors duration-200 dark:hover:text-indigo-500 dark:text-gray-300 hover:text-indigo-500 focus:outline-none">
                                <CollectionUpdate
                                  data={data}
                                  id={collection._id}
                                  categoryData={category}
                                />
                              </div>
                              <button
                                onClick={() => handleRemove(collection)}
                                className="text-blue-500 transition-colors duration-200 hover:text-indigo-500 focus:outline-none"
                              >
                                Xóa
                              </button>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default Collection;
