/* eslint-disable @typescript-eslint/no-explicit-any */
import { faChevronDown, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import React, { useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import { useGlobalState } from "../../../../common/context/AppContextProvider";
import useCollectionQuery from "../../../../common/hooks/Collection/useCollectionQuery";
import { ICategory } from "../../../../common/interfaces/Category";
import { AuthLogOut } from "../../../../services/auth";
import MenuHover from "../../../ui/MenuHover";
import Search from "../Search";
import "./style.css";

import { toast } from "react-toastify";
const Header = () => {
  const location = useLocation();
  const isHomePage = location.pathname === "/";
  const [isScrolled, setIsScrolled] = useState(false);
  const { data: collections } = useCollectionQuery();
  const [hoodie, setHoodie] = useState([]);
  const [tShirt, setTShirt] = useState([]);
  const { state, dispatch } = useGlobalState();
  const navigate = useNavigate();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.scrollY;
      const menu = document.getElementById("menu");

      if (menu) {
        const menuOffsetTop = menu.offsetTop;
        const menuHeight = menu.clientHeight;

        if (scrollTop > menuOffsetTop + menuHeight) {
          setIsScrolled(true);
        } else {
          setIsScrolled(false);
        }
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (collections) {
      // Lọc các bộ sưu tập có danh mục là "hoodie" và "t-shirt"
      const hoodieCollections = collections.filter((collection: any) => {
        return collection.category.some(
          (category: ICategory) => category.name === "Hoodie"
        );
      });
      setHoodie(hoodieCollections);
      const tShirtCollections = collections.filter((collection: any) => {
        return collection.category.some(
          (category: ICategory) => category.name === "T-shirt"
        );
      });
      setTShirt(tShirtCollections);
    }
  }, [collections]);
  const handleLogout = async () => {
    const res = await AuthLogOut();
    console.log(res);
    dispatch({ type: "LOGOUT" });
    toast("Đăng xuất thành công !", {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
    navigate("/");
  };
  return (
    <header
      className={`w-full z-50 flex justify-center ${
        isScrolled ? "transition fixed m-5" : ""
      } ${isHomePage ? "fixed" : "m-0"}`}
    >
      <div
        id="menu"
        className={`container flex items-center justify-between w-full p-4

        ${
          isScrolled
            ? "text-black bg-white transition shadow-2xl rounded-2xl"
            : `${
                isHomePage ? "text-white" : "text-black"
              } bg-none transition rounded-2xl`
        }
        `}
      >
        <div className="logo">
          <Link to="/">
            <img
              src="https://cottonusa.co/cdn/shop/files/COTTON_USA_LOGO_02.png?v=1691657951&width=240"
              alt="Cotton USA"
              width="120px"
              height="49px"
            />
          </Link>
        </div>
        <div className="menu-center">
          <nav className="flex items-center justify-center gap-8">
            <ul className="text-lg font-bold">
              <Link to="/">Trang chủ</Link>
            </ul>
            <ul className="text-lg font-bold">
              <Link to="/collection/t-shirt"> T-shirts</Link>
            </ul>
            <ul className="text-lg font-bold flex items-center">
              <MenuHover FlyoutContent={hoodie}>Hoodies</MenuHover>
              <FontAwesomeIcon className="text-sm pl-2" icon={faChevronDown} />
            </ul>
            <ul className="text-lg font-bold">
              <MenuHover FlyoutContent={tShirt}>Collection</MenuHover>
            </ul>
          </nav>
        </div>
        <div className="featured flex item-center gap-3">
          <Search />
          {state.user ? (
            <>
              <button
                id="basic-button"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
                className="p-0  "
              >
                <FontAwesomeIcon icon={faUser} />
              </button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem onClick={handleClose}>
                  <Link to="/">Đơn hàng của tôi</Link>
                </MenuItem>
                {state.user.role === "admin" ? (
                  <MenuItem onClick={handleClose}>
                    <Link to="/admin">Quản trị trang web</Link>
                  </MenuItem>
                ) : (
                  <MenuItem onClick={handleClose}>
                    <Link to="/">Tài khoản</Link>
                  </MenuItem>
                )}
                <MenuItem onClick={handleClose}>
                  <button onClick={handleLogout}>Đăng xuất</button>
                </MenuItem>
              </Menu>
            </>
          ) : (
            <Link className="text-lg relative" to="/sign-in">
              <FontAwesomeIcon icon={faUser} />
            </Link>
          )}

          <svg
            role="presentation"
            strokeWidth="2"
            focusable="false"
            width="22"
            height="22"
            className="icon icon-cart"
            viewBox="0 0 22 22"
          >
            <path
              d="M11 7H3.577A2 2 0 0 0 1.64 9.497l2.051 8A2 2 0 0 0 5.63 19H16.37a2 2 0 0 0 1.937-1.503l2.052-8A2 2 0 0 0 18.422 7H11Zm0 0V1"
              fill="none"
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
            ></path>
          </svg>
        </div>
      </div>
    </header>
  );
};

export default Header;
