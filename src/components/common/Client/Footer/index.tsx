import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faInstagram,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { Link } from "react-router-dom";
import FooterLink from "./FooterLink";

const Footer = () => {
  return (
    <footer className="bg-gradient-to-r from-gray-100 via-[#bce1ff] to-gray-100">
      <div className="container px-6 py-12 mx-auto">
        <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 sm:gap-y-10 lg:grid-cols-5">
          <div className="sm:col-span-2">
            <h1 className="max-w-lg text-xl font-semibold tracking-tight text-gray-800 xl:text-2xl dark:text-white">
              Đăng ký để nhận ưu đãi đặc biệt, free giveaways và các thông tin
              khác.
            </h1>
            <div className="flex flex-col mx-auto mt-6 space-y-3 md:space-y-0 md:flex-row">
              <form action="">
                <input
                  id="email"
                  type="text"
                  className="px-4 py-2 text-gray-700 bg-white border rounded-md dark:bg-gray-900 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 dark:focus:border-blue-300 focus:outline-none focus:ring focus:ring-opacity-40 focus:ring-blue-300"
                  placeholder="Email Address"
                />
                <button className="w-full px-6 py-2.5 text-sm font-medium tracking-wider text-white transition-colors duration-300 transform md:w-auto md:mx-4 focus:outline-none bg-gray-800 rounded-lg hover:bg-gray-700 focus:ring focus:ring-gray-300 focus:ring-opacity-80">
                  Đăng kí
                </button>
              </form>
            </div>
          </div>
          <div>
            <p className="font-semibold text-gray-800 dark:text-white">
              Thông tin
            </p>
            <div className="flex flex-col items-start mt-5 space-y-2">
              <FooterLink href="/" value="About us" />
              <FooterLink href="/" value="Liên hệ chúng tôi" />
            </div>
          </div>
          <div>
            <p className="font-semibold text-gray-800 dark:text-white">
              Hỗ trợ khách hàng
            </p>
            <div className="flex flex-col items-start mt-5 space-y-2">
              <FooterLink href="/" value="Chính sách đổi trả & bảo hành" />
              <FooterLink href="/" value="Chính sách vận chuyển" />
              <FooterLink href="/" value="Điều khoản dịch vụ" />
              <FooterLink href="/" value="Chính sách bảo mật" />
            </div>
          </div>
          <div>
            <p className="font-semibold text-gray-800 dark:text-white">
              Dịch vụ khách hàng
            </p>
            <div className="flex flex-col items-start mt-5 space-y-2">
              <FooterLink href="tel:+4733378901" value="Hotline: 0326171968" />
              <FooterLink
                href="mailto:webmaster@example.com"
                value="Email: support@cottonusa.co"
              />
              <p className="text-gray-600 transition-colors duration-300 ">
                Thứ Hai - Chủ nhật 08:00 ~ 22:00{" "}
              </p>
            </div>
          </div>
        </div>
        <hr className="my-6 border-gray-200 md:my-8 dark:border-gray-700" />
        <div className="flex items-center justify-between">
          <div className="flex flex-col">
            <Link className="mb-2" to="#">
              <img
                className="w-auto h-7"
                src="src/resources/images/Logo/COTTON_USA_LOGO.webp"
                alt=""
              />
            </Link>
            <span className="text-gray-600 text-sm transition-colors duration-300 dark:text-gray-3000">
              © {new Date().getFullYear()}. All Rights Reserved.
            </span>
          </div>
          <div className="flex -mx-2">
            <a
              href="#"
              className="mx-2 text-gray-600 transition-colors duration-300 dark:text-gray-300 hover:text-blue-500 dark:hover:text-blue-400"
              aria-label="Facebook"
            >
              <FontAwesomeIcon icon={faYoutube} />
            </a>
            <a
              href="#"
              className="mx-2 text-gray-600 transition-colors duration-300 dark:text-gray-300 hover:text-blue-500 dark:hover:text-blue-400"
              aria-label="Reddit"
            >
              <FontAwesomeIcon icon={faFacebook} />
            </a>
            <a
              href="#"
              className="mx-2 text-gray-600 transition-colors duration-300 dark:text-gray-300 hover:text-blue-500 dark:hover:text-blue-400"
              aria-label="Github"
            >
              <FontAwesomeIcon icon={faInstagram} />
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
