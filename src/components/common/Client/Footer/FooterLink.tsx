type FooterLink = {
  href?: string;
  value: string;
};
const FooterLink = ({ href, value }: FooterLink) => {
  return (
    <a
      href={href}
      className="text-gray-600 transition-colors duration-300 dark:text-gray-300 dark:hover:text-blue-400 hover:underline"
    >
      {value}
    </a>
  );
};

export default FooterLink;
