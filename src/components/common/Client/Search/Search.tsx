import TextField from "@mui/material/TextField";

const SearchComponent = () => {
  return (
    <>
      <form className="w-full">
        <TextField
          sx={{
            width: "100%",
            fontSize: "16px",
          }}
          id="standard-basic"
          placeholder="Tìm kiếm..."
          variant="standard"
        />
      </form>
    </>
  );
};
export default SearchComponent;
