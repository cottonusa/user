// SwipeableTemporaryDrawer.tsx
import * as React from "react";
import Box from "@mui/material/Box";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import Button from "@mui/material/Button";
import SearchComponent from "./Search";
import "./style.css";

type Anchor = "right";

export default function Search() {
  const [state, setState] = React.useState({
    right: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  const anchor: Anchor = "right";

  return (
    <div>
      <Button
        sx={{ color: "inherit", padding: 0, minWidth: "auto" }}
        onClick={toggleDrawer(anchor, true)}
      >
        <svg
          role="presentation"
          strokeWidth="2"
          focusable="false"
          width="22"
          height="22"
          className="icon icon-search"
          viewBox="0 0 22 22"
        >
          <circle
            cx="11"
            cy="10"
            r="7"
            fill="none"
            stroke="currentColor"
          ></circle>
          <path
            d="m16 15 3 3"
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
          ></path>
        </svg>
      </Button>
      <SwipeableDrawer
        id="drawer"
        anchor={anchor}
        open={state[anchor]}
        onClose={toggleDrawer(anchor, false)}
        onOpen={toggleDrawer(anchor, true)}
      >
        <Box className="p-5 m-4 " sx={{ width: 680 }}>
          <SearchComponent />
        </Box>
      </SwipeableDrawer>
    </div>
  );
}
