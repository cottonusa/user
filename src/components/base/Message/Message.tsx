import { useState, useEffect } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

type Props = {
  type: "info" | "success" | "warning" | "error";
  message: string;
  timeout: number;
  openMessage: boolean;
};

const Message = ({ type, message, timeout, openMessage = false }: Props) => {
  const [open, setOpen] = useState(openMessage);

  useEffect(() => {
    setOpen(openMessage);
  }, [openMessage]);

  useEffect(() => {
    if (open) {
      switch (type) {
        case "info":
          toast.info(message, {
            position: "top-center",
            autoClose: timeout,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          break;
        case "success":
          toast.success(message, {
            position: "top-center",
            autoClose: timeout,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          break;
        case "warning":
          toast.warning(message, {
            position: "top-center",
            autoClose: timeout,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          break;
        case "error":
          toast.error(message, {
            position: "top-center",
            autoClose: timeout,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          break;
        default:
          break;
      }
    }
  }, [open, message, timeout, type]);

  return null;
};

export default Message;
