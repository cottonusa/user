import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";
import { Fragment, useState } from "react";
import { ICollection } from "../../common/interfaces/Collection";
type SelectOptionProps = {
  title: string;
  values: OptionProps[];
};
interface OptionProps {
  _id?: string;
  name?: string;
}
function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

const SelectOption = ({ title, values, onChangeValue }: SelectOptionProps) => {
  const [selected, setSelected] = useState<ICollection>();
  return (
    <div className="sm:col-span-2">
      <label
        htmlFor={title}
        className="block text-sm font-medium leading-6 text-gray-900"
      >
        {title}
      </label>
      <Listbox value={selected} onChange={setSelected}>
        {({ open }) => (
          <>
            <div className="relative mt-2">
              <Listbox.Button className="relative cursor-pointer w-full sm:max-w-sm rounded-md bg-white py-1.5 pl-3 pr-10 text-left text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6">
                {/* <Listbox.Button className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6"> */}
                <span className="flex items-center">
                  <span className="block truncate">
                    {selected ? selected.name : "Select..."}
                  </span>
                </span>
                <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                  <ChevronUpDownIcon
                    className="h-5 w-5 text-gray-400"
                    aria-hidden="true"
                  />
                </span>
              </Listbox.Button>

              <Transition
                show={open}
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Listbox.Options className="absolute z-10 mt-1 max-h-56  w-full sm:max-w-sm  overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                  {values.map((value, index: number) => (
                    <Listbox.Option
                      key={index}
                      className={({ active }) =>
                        classNames(
                          active ? "bg-indigo-600 text-white" : "text-gray-900",
                          "relative cursor-default select-none py-2 pl-3 pr-9"
                        )
                      }
                      value={value}
                    >
                      {({ selected, active }) => (
                        <>
                          <div>
                            <span
                              className={classNames(
                                selected ? "font-semibold" : "font-normal",
                                " block truncate"
                              )}
                            >
                              {value.name}
                            </span>
                          </div>

                          {selected ? (
                            <span
                              className={classNames(
                                active ? "text-white" : "text-indigo-600",
                                "absolute inset-y-0 right-0 flex items-center pr-4"
                              )}
                            >
                              <CheckIcon
                                className="h-5 w-5"
                                aria-hidden="true"
                              />
                            </span>
                          ) : null}
                        </>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </>
        )}
      </Listbox>
    </div>
  );
};
export default SelectOption;
