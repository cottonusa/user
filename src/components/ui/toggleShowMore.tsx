import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";

import React, { useState } from "react";

interface ToggleShowMoreProps {
  description: string;
  title: string;
}

const ToggleShowMore = ({ description, title }: ToggleShowMoreProps) => {
  const [showMore, setShowMore] = useState<boolean>(false);

  const handleToggleShowMore = () => {
    setShowMore(!showMore);
  };

  return (
    <div>
      <div>
        <button onClick={handleToggleShowMore}>
          {title}
          <FontAwesomeIcon
            className="bg-gray-300 p-1 rounded-xl text-xs w-auto hover:bg-black hover:text-white transition"
            icon={showMore ? faChevronUp : faChevronDown}
          />
        </button>
      </div>
      {showMore && (
        <div>
          <p>{description}</p>
        </div>  
      )}
    </div>
  );
};

export default ToggleShowMore;
