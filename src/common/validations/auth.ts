import Joi from "joi";

export const SignUpSchema = Joi.object({
  firstName: Joi.string().required().min(3).empty().messages({
    "any.required": "Họ là bắt buộc.",
    "string.min": "Họ phải có ít nhất {#limit} ký tự.",
    "string.empty": "Vui lòng nhập đầy đủ thông tin",
  }),
  lastName: Joi.string().required().min(3).empty().messages({
    "any.required": "Tên là bắt buộc.",
    "string.min": "Tên phải có ít nhất {#limit} ký tự.",
    "string.empty": "Vui lòng nhập đầy đủ thông tin",
  }),
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .min(3)
    .required()
    .empty()
    .messages({
      "string.email": "Vui lòng cung cấp địa chỉ email hợp lệ.",
      "any.required": "Email là bắt buộc.",
      "string.min": "Email phải có ít nhất {#limit} ký tự.",
      "string.empty": "Vui lòng nhập đầy đủ thông tin",
    }),
  password: Joi.string().min(6).required().empty().messages({
    "any.required": "Mật khẩu là bắt buộc.",
    "string.empty": "Vui lòng nhập đầy đủ thông tin",
    "string.min": "Mật khẩu phải có ít nhất {#limit} ký tự.",
  }),
  confirmPassword: Joi.string()
    .valid(Joi.ref("password"))
    .required()
    .label("Confirm password")
    .messages({
      "any.required": "Mật khẩu là bắt buộc.",
      "any.only": "Mật khẩu xác nhận phải khớp với mật khẩu đã nhập.",
    }),
});

export const SignInSchema = Joi.object({
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .min(6)
    .required()
    .messages({
      "string.email": "Vui lòng cung cấp địa chỉ email hợp lệ.",
      "any.required": "Email là trường bắt buộc.",
      "string.min": "Email phải có độ dài tối thiểu {#limit} ký tự.",
    }),
  password: Joi.string().min(6).required().messages({
    "any.required": "Mật khẩu là bắt buộc.",
    "string.min": "Mật khẩu phải có ít nhất {#limit} ký tự.",
  }),
});

