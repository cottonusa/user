import { useQuery } from "@tanstack/react-query";
import { get, getById } from "../../../services/auth";


const useAuthQuery = (id?: string) => {
  const { data, ...rest } = useQuery({
    queryKey: ["AUTH_KEY", id],
    queryFn: async () => {
      return id ? await getById(id) : await get();
    },
  });
  return { data, ...rest };
};
export default useAuthQuery;
