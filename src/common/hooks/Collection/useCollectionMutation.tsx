import { joiResolver } from "@hookform/resolvers/joi";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { SubmitHandler, useForm } from "react-hook-form";
import Message from "../../../components/base/Message/Message";
import { ICollection } from "../../interfaces/Collection";
import { CategoryJoiSchema } from "../../validations/category";
import { create, remove, update } from "../../../services/collection";

type useCollectionMutationProps = {
  action: "CREATE" | "DELETE" | "UPDATE";
  onSuccess?: () => void;
};

const useCollectionMutation = ({
  action,
  onSuccess,
}: useCollectionMutationProps) => {
  const queryClient = useQueryClient();
  const form = useForm({
    resolver: joiResolver(CategoryJoiSchema),
    defaultValues: {
      name: "",
      category: []
    },
  });

  const { mutate, ...rest } = useMutation({
    mutationFn: async (collection: ICollection) => {
      switch (action) {
        case "CREATE":
          return await create(collection);
        case "DELETE":
          return await remove(collection);
        case "UPDATE":
          console.log(123);
          return await update(collection);
        default:
          return null;
      }
    },
    onSuccess: (data) => {
      if (data) {
        onSuccess && onSuccess();
        queryClient.invalidateQueries({
          queryKey: ["COLLECTION_KEY"],
        });
      } else {
        <Message
          message={"Có lỗi xảy ra vui lòng thử lại !"}
          timeout={5000}
          openMessage={true}
          type={"error"}
        />;
        return;
      }
    },
    onError: (error) => {
      console.log(error);
    },
  });
  const onSubmit: SubmitHandler<ICollection> = async (collection) => {
    mutate(collection);
    console.log(collection);
  };

  return { mutate, form, onSubmit, ...rest };
};

export default useCollectionMutation;
