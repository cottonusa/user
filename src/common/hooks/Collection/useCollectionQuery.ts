
import { useQuery } from "@tanstack/react-query";
import { get, getById } from "../../../services/collection";
const useCollectionQuery = (id?: string) => {
  const { data, ...rest } = useQuery({
    queryKey: ["COLLECTION_KEY", id],
    queryFn: async () => {
      return id ? await getById(id) : await get();
    },
  });
  return { data, ...rest };
};
export default useCollectionQuery;
