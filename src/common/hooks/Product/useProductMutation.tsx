import { joiResolver } from "@hookform/resolvers/joi";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { SubmitHandler, useForm } from "react-hook-form";
import Message from "../../../components/base/Message/Message";
import { CategoryJoiSchema } from "../../validations/category";
import { IProduct } from "../../interfaces/Product";
import { create, remove, update } from "../../../services/product";

type useProductMutationProps = {
  action: "CREATE" | "DELETE" | "UPDATE";
  onSuccess?: () => void;
};

const useProductMutation = ({ action, onSuccess }: useProductMutationProps) => {
  const queryClient = useQueryClient();
  const form = useForm({
    resolver: joiResolver(CategoryJoiSchema),
    defaultValues: {
      name: "",
      description: "",
      price: "",
      discount: "",
      material: "",
      variants: [
        {
          color: "",
          option: [
            {
              size: "",
              quantity: 0,
              thumbnail: [
                {
                  imageUrl: "",
                  isBackground: false,
                },
              ],
            },
          ],
        },
      ],
      sku: "",
      collections: "",
    },
  });

  const { mutate, ...rest } = useMutation({
    mutationFn: async (product: IProduct) => {
      switch (action) {
        case "CREATE":
          return await create(product);
        case "DELETE":
          return await remove(product);
        case "UPDATE":
          console.log(123);
          return await update(product);
        default:
          return null;
      }
    },
    onSuccess: (data) => {
      if (data) {
        onSuccess && onSuccess();
        queryClient.invalidateQueries({
          queryKey: ["PRODUCT_KEY"],
        });
      } else {
        <Message
          message={"Có lỗi xảy ra vui lòng thử lại !"}
          timeout={5000}
          openMessage={true}
          type={"error"}
        />;
        return;
      }
    },
    onError: (error) => {
      console.log(error);
    },
  });
  const onSubmit: SubmitHandler<IProduct> = async (product) => {
    mutate(product);
    console.log(product);
  };

  return { mutate, form, onSubmit, ...rest };
};

export default useProductMutation;
