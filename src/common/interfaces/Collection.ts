import { ReactNode } from "react";

export interface ICollection {
  _id?: string;
  name?: string;
  products?: string[];
  category?: Category[];
  slug?: string;
  createdAt?: ReactNode;
  updatedAt?: ReactNode;
}
interface Category {
  _id: string;
  name: string;
}
