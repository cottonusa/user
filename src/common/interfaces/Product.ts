import { ReactNode } from "react";

export interface IProduct {
  _id?: string;
  name: string;
  description: string;
  price: number;
  material: string;
  discount?: number;
  variants: {
    color: string;
    thumbnail: {
      imageUrl: string;
    }[];
    option: {
      size: string;
      quantity: number;
    }[];
  }[];
  collections: Collection;
  category: Category;
  sku: string;
  slug?: string;
  createdAt: ReactNode;
  updatedAt?: ReactNode;
}
interface Category {
  name: string;
}
interface Collection {
  _id: string;
  name: string;
  category: Category;
  slug?: string;
}
