import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { GlobalStateProvider } from "../common/context/AppContextProvider";
import AdminLayout from "../layouts/AdminLayout";
import ClientLayout from "../layouts/ClientLayout";
import Dashboard from "../pages/Admin/Dashboard/Dashboard";
import ProductList from "../pages/Admin/Product/List/List";
import ProductUpdate from "../pages/Admin/Product/Update/Update";
import SignIn from "../pages/Client/Auth/SignIn";
import SignUp from "../pages/Client/Auth/SignUp";
import Cart from "../pages/Client/Cart/Cart";
import Home from "../pages/Client/Home";
import ProductDetail from "../pages/Client/Products/Detail/ProductDetail";
import Category from "../pages/Admin/Category/List";
import Collections from "../pages/Client/Collection/Collection";
import Collection from "../pages/Admin/Collection/List";
import ProductCreate from "../pages/Admin/Product/Create/Create";

const RouterComponent = () => {
  return (
    <>
      <Router>
        <GlobalStateProvider>
          <Routes>
            <Route path="/" element={<ClientLayout />}>
              <Route index element={<Home />} />
              {/* <Route path="/collection/t-shirts" element={<Collection />} /> */}
              <Route path="/collection/:slug" element={<Collections />} />
              <Route path="/product/:slug" element={<ProductDetail />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/sign-in" element={<SignIn />} />
              <Route path="/sign-up" element={<SignUp />} />
            </Route>
            <Route path="/admin" element={<AdminLayout />}>
              <Route index element={<Dashboard />} />
              <Route path="/admin/product" element={<ProductList />} />
              <Route path="/admin/product/create" element={<ProductCreate />} />
              <Route
                path="/admin/product/update/:id"
                element={<ProductUpdate />}
              />
              <Route path="/admin/category" element={<Category />} />
              <Route path="/admin/collection" element={<Collection />} />
            </Route>
          </Routes>
        </GlobalStateProvider>
      </Router>
    </>
  );
};

export default RouterComponent;
