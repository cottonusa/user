import { ICollection } from "../common/interfaces/Collection";
import instance from "../configs/axios";

export const get = async () => {
  try {
    const { data } = await instance.get(`/collection`);
    return data.data;
  } catch (error) {
    console.log(error);
  }
};

export const getById = async (id: string) => {
  try {
    const { data } = await instance.get(`/collection/${id}`);
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const create = async (collection: ICollection) => {
  try {
    const { data } = await instance.post("/collection", collection);
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const remove = async (collection: ICollection) => {
  try {
    const { data } = await instance.delete(`/collection/${collection._id}`);
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const update = async (collection: ICollection) => {
  try {
    const { data } = await instance.put(
      `/collection/${collection._id}`,
      collection
    );
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
};
