import { IProduct } from "../common/interfaces/Product";
import instance from "../configs/axios";

export const get = async (page?: number, limit?: number) => {
  try {
    const { data } = await instance.get(
      `/product?_page=${page}&_limit=${limit}`
    );

    return data.data.docs;
  } catch (error) {
    console.log(error);
  }
};

export const getBySlug = async (slug: string) => {
  try {
    const { data } = await instance.get(`/product/${slug}`);
    return data.data;
  } catch (error) {
    console.log(error);
  }
};

export const create = async (product: IProduct) => {
  try {
    const { data } = await instance.post("/product", product);
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const remove = async (product: IProduct) => {
  try {
    const { data } = await instance.delete(`/product/${product._id}`);
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const update = async (product: IProduct) => {
  try {
    const { data } = await instance.put(`/product/${product._id}`, product);
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
};
