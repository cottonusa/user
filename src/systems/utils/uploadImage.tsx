import axios, { AxiosResponse } from "axios";
import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}
const uploadFileCloudinary = async (file: File) => {
  try {
    const formData = new FormData();
    formData.append("file", file);
    formData.append("upload_preset", "cottonUSA"); // Thay bằng upload preset của bạn
    formData.append("folder", "cottonUSA");
    const response = await axios.post(
      "https://api.cloudinary.com/v1_1/dou8bu6ei/upload", // Thay bằng cloudinary name của bạn
      formData
    );
    return response.data.url;
  } catch (error) {
    // handle error here
    console.error(error);
  }
};
const CLOUDINARY_API_KEY = "your_cloudinary_api_key";
const CLOUDINARY_API_SECRET = "your_cloudinary_api_secret";
const CLOUDINARY_CLOUD_NAME = "your_cloudinary_cloud_name";

async function deleteImageFromCloudinary(publicId: string): Promise<void> {
  try {
    const response: AxiosResponse = await axios.delete(
      `https://api.cloudinary.com/v1_1/${CLOUDINARY_CLOUD_NAME}/image/${publicId}`,
      {
        auth: {
          username: CLOUDINARY_API_KEY,
          password: CLOUDINARY_API_SECRET,
        },
      }
    );

    if (response.status === 200) {
      console.log(`Image with public ID ${publicId} deleted successfully.`);
    } else {
      console.error(
        `Error deleting image with public ID ${publicId}:`,
        response.data
      );
    }
  } catch (error) {
    console.error("Error deleting image from Cloudinary:", error);
    throw error;
  }
}
export { uploadFileCloudinary, deleteImageFromCloudinary };
